
import corba.EchoServicePOA;
import org.omg.CORBA.ORB;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author jonathan
 */
public class EchoImpl extends EchoServicePOA{
    
      private ORB orb;
    
    public void setORB(ORB orb_val){
        orb = orb_val;
    
    }

    @Override
    public String echo(String x) {
        return x + " ===> echo";
    }
    
}
